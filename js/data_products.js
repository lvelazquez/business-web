
			var category_list = [
				'Bolsos Viajeros-BV',
				'Bolsos Deportivos-BD',
				'Morrales-MO',
				'Equipos de Montaña-HK',
				'Cartucheras-CA',
				'Gorras-GO',
				'Sombreros-SO',
				'Bolsos Playeros-BP',
				'Esterillas-TO',
				'Bolsas Tulas-TU',
				'Maletines-CR',
				'Articulos en Cuero-CU',
				'Maletines-MA',
				'Portafolios-PF',
				'Agendas-AG',
				'Koalas-KO',
				'Porta Cosmeticos-PC',
				'Térmicos-AL',
				'Cavas Delivery-CD',
				'Sillas-SI',
				'Maletines con Ruedas-RU',
				'Maletines Primeros Auxilios-PA',
				'Portatrajes-PT',
				'Equipos Militares-EM',
				'Combos-CC',
				'Accesorios-AC',
				'Coolers-CO',
				'Tazas y Vasos-MU',
				'Franelas y Camisas-CM',
				'Chemises y Chaquetas-CH'
			]




	var data = {"BD": ["BD-24", "BD-25", "BD-26", "BD-27", "BD-20", "BD-21", "BD-22", "BD-23", "BD-28", "BD-15", "BD-14", "BD-17", "BD-16", "BD-11", "BD-10", "BD-13", "BD-12", "BD-19", "BD-18", "BD-06", "BD-07", "BD-04", "BD-02", "BD-03", "BD-01", "BD-08", "BD-09"], "BT": ["BT-06", "BT-07", "BT-04", "BT-05", "BT-02", "BT-03", "BT-01", "BT-08", "BT-09", "BT-43", "BT-40", "BT-46", "BT-44", "BT-45", "BT-33", "BT-32", "BT-31", "BT-30", "BT-36", "BT-35", "BT-39", "BT-38", "BT-28", "BT-29", "BT-24", "BT-25", "BT-26", "BT-27", "BT-20", "BT-21", "BT-22", "BT-23", "BT-60", "BT-62", "BT-14", "BT-17", "BT-11", "BT-10", "BT-13", "BT-12", "BT-19", "BT-18", "BT-34", "BT-59", "BT-58", "BT-55", "BT-54", "BT-56", "BT-SI-09"], "HK": ["HK-05", "HK-06", "HK-09", "HK-08", "HK-10", "HK-01", "HK-02", "HK-07"], "BV": ["BV-01", "BV-02", "BV-03", "BV-04", "BV-05", "BV-06", "BV-07", "BV-08"], "BP": ["BP-11", "BP-10", "BP-13", "BP-12", "BP-15", "BP-14", "BP-17", "BP-16", "BP-19", "BP-18", "BP-28", "BP-02", "BP-03", "BP-01", "BP-06", "BP-07", "BP-04", "BP-05", "BP-08", "BP-09", "BP-37", "BP-36", "BP-32", "BP-31", "BP-30", "BP-35", "BP-34", "BP-33", "BP-29", "BP-20", "BP-21", "BP-22", "BP-23", "BP-24", "BP-25", "BP-26", "BP-27"], "RU": ["RU-12", "RU-13", "RU-11", "RU-14", "RU-15", "RU-03", "RU-07", "RU-06", "RU-09"], "CM": ["CM-12", "CM-18", "CM-08", "CM-09", "CM-04", "CM-05", "CM-06", "CM-07", "CM-01", "CM-02", "CM-03", "CM-13", "CM-11", "CM-10", "CM-17", "CM-16", "CM-15", "CM-14"], "PT": ["PT-01", "PT-02", "PT-03"], "PC": ["PC-74", "PC-70", "PC-14", "PC-49", "PC-48", "PC-45", "PC-44", "PC-47", "PC-46", "PC-41", "PC-40", "PC-43", "PC-42", "PC-38", "PC-39", "PC-30", "PC-05", "PC-32", "PC-33", "PC-34", "PC-35", "PC-36", "PC-04", "PC-08", "PC-01", "PC-03", "PC-02", "PC-07", "PC-06", "PC-31", "PC-59", "PC-56", "PC-57", "PC-54", "PC-55", "PC-52", "PC-53", "PC-50", "PC-51", "PC-37", "PC-60", "PC-72", "PC-73", "PC-09", "PC-71", "PC-28", "PC-21", "PC-20", "PC-12", "PC-13", "PC-10", "PC-11", "PC-17", "PC-69", "PC-68", "PC-63", "PC-62", "PC-61", "PC-67", "PC-66", "PC-65", "PC-64", "PC-58", "PC-16", "PC-15", "PC-18", "PC-19", "PC-29", "PC-27", "PC-26", "PC-25", "PC-24", "PC-23", "PC-22", "PC-1"], "TU": ["TU-03", "TU-02", "TU-01", "TU-06", "TU-05", "TU-04", "TU-09", "TU-08", "TU-18", "TU-19", "TU-14", "TU-15", "TU-16", "TU-17", "TU-10", "TU-11", "TU-12", "TU-13", "TU-21", "TU-23", "TU-22", "TU-20"], "TO": ["TO-01", "TO-03", "TO-02"], "PA": ["PA-09", "PA-08", "PA-03", "PA-02", "PA-01", "PA-07", "PA-05", "PA-04", "PA-10", "PA-11", "PA-12"], "PF": ["PF-10", "PF-13", "PF-12", "PF-15", "PF-14", "PF-17", "PF-16", "PF-19", "PF-18", "PF-11", "PF-02", "PF-03", "PF-01", "PF-07", "PF-04", "PF-05", "PF-08", "PF-09", "PF-06"], "EM": ["EM-02", "EM-03", "EM-01"], "VA": ["VA-01", "VA-03", "VA-02", "VA-04"], "AC": ["AC-39", "AC-28", "AC-26", "AC-27", "AC-24", "AC-25", "AC-14", "AC-13", "AC-12", "AC-11", "AC-10", "AC-48", "AC-45", "AC-46", "AC-47", "AC-42", "AC-43", "AC-02", "AC-09"], "CH": ["CH-09", "CH-03", "CH-02", "CH-01", "CH-07", "CH-06", "CH-05", "CH-04", "CH-08", "CH-10", "CH-11", "CH-12", "CH-13"], "CO": ["CO-13", "CO-11", "CO-10", "CO-12", "CO-14", "CO-02", "CO-03", "CO-01", "CO-06", "CO-07", "CO-04", "CO-05", "CO-08", "CO-09"], "AG": ["AG-04", "AG-05", "AG-01", "AG-02", "AG-03"], "CC": ["CC-08", "CC-06", "CC-07", "CC-04", "CC-05", "CC-02", "CC-03", "CC-01"], "CA": ["CA-08", "CA-09", "CA-01", "CA-02", "CA-03", "CA-04", "CA-05", "CA-06", "CA-07", "CA-14", "CA-13", "CA-12", "CA-11", "CA-10"], "AL": ["AL-23", "AL-22", "AL-21", "AL-20", "AL-27", "AL-26", "AL-25", "AL-24", "AL-29", "AL-28", "AL-30", "AL-31", "AL-05", "AL-04", "AL-07", "AL-06", "AL-01", "AL-03", "AL-02", "AL-09", "AL-08", "AL-15", "AL-16", "AL-14", "AL-17", "AL-12", "AL-13", "AL-10", "AL-11", "AL-18", "AL-19"], "CD": ["CD-09", "CD-08", "CD-07", "CD-06", "CD-05", "CD-04", "CD-03", "CD-02", "CD-01", "CD-10", "CD-11", "CD-12", "CD-13", "CD-14"], "GO": ["GO-20", "GO-21", "GO-22", "GO-15", "GO-14", "GO-17", "GO-16", "GO-11", "GO-19", "GO-18", "GO-10", "GO-13", "GO-12", "GO-08", "GO-09", "GO-06", "GO-07", "GO-04", "GO-05", "GO-02", "GO-03", "GO-01"], "CR": ["CR-09", "CR-08", "CR-01", "CR-03", "CR-02", "CR-05", "CR-04", "CR-07", "CR-06", "CR-12", "CR-13", "CR-10", "CR-11", "CR-16", "CR-14", "CR-15"], "CU": ["CU-22", "CU-20", "CU-21", "CU-19", "CU-18", "CU-13", "CU-12", "CU-11", "CU-10", "CU-17", "CU-16", "CU-15", "CU-14", "CU-04", "CU-05", "CU-06", "CU-07", "CU-01", "CU-02", "CU-03", "CU-08", "CU-09"], "MA": ["MA-37", "MA-36", "MA-35", "MA-34", "MA-33", "MA-32", "MA-47", "MA-44", "MA-28", "MA-29", "MA-20", "MA-21", "MA-22", "MA-23", "MA-24", "MA-25", "MA-26", "MA-27", "MA-02", "MA-03", "MA-39", "MA-38", "MA-31", "MA-30", "MA-11", "MA-10", "MA-13", "MA-12", "MA-15", "MA-14", "MA-17", "MA-16", "MA-19", "MA-18", "MA-41", "MA-40", "MA-46", "MA-45", "MA-42", "MA-43", "MA-01", "MA-06", "MA-07", "MA-04", "MA-05", "MA-08", "MA-09"], "MO": ["MO-01", "MO-02", "MO-03", "MO-04", "MO-05", "MO-08", "MO-09", "MO-71", "MO-70", "MO-73", "MO-72", "MO-06", "MO-31", "MO-30", "MO-33", "MO-32", "MO-35", "MO-34", "MO-37", "MO-36", "MO-39", "MO-38", "MO-69", "MO-66", "MO-67", "MO-64", "MO-65", "MO-62", "MO-63", "MO-60", "MO-61", "MO-68", "MO-19", "MO-18", "MO-13", "MO-12", "MO-22", "MO-23", "MO-20", "MO-21", "MO-26", "MO-27", "MO-24", "MO-25", "MO-28", "MO-29", "MO-59", "MO-58", "MO-53", "MO-52", "MO-51", "MO-50", "MO-57", "MO-56", "MO-55", "MO-54", "MO-17", "MO-16", "MO-15", "MO-14", "MO-11", "MO-10", "MO-44", "MO-45", "MO-46", "MO-47", "MO-40", "MO-41", "MO-42", "MO-48", "MO-49", "MO-07"], "KO": ["KO-33", "KO-32", "KO-30", "KO-19", "KO-18", "KO-11", "KO-10", "KO-13", "KO-12", "KO-15", "KO-14", "KO-17", "KO-16", "KO-46", "KO-47", "KO-44", "KO-45", "KO-42", "KO-43", "KO-40", "KO-41", "KO-48", "KO-49", "KO-08", "KO-09", "KO-02", "KO-03", "KO-01", "KO-06", "KO-07", "KO-04", "KO-05", "KO-70", "KO-37", "KO-36", "KO-35", "KO-34", "KO-31", "KO-39", "KO-38", "KO-68", "KO-69", "KO-64", "KO-65", "KO-66", "KO-67", "KO-60", "KO-61", "KO-62", "KO-63", "KO-20", "KO-21", "KO-22", "KO-23", "KO-24", "KO-25", "KO-26", "KO-27", "KO-28", "KO-29", "KO-55", "KO-54", "KO-57", "KO-56", "KO-51", "KO-50", "KO-53", "KO-52", "KO-59", "KO-58"], "MU": ["MU-04G", "MU-04P", "MU-10P", "MU-10G", "MU-11", "MU-06", "MU-07", "MU-05", "MU-02", "MU-03", "MU-01", "MU-08", "MU-09"], "SI": ["SI-03", "SI-11", "SI-10", "SI-01", "SI-02", "SI-04", "SI-05", "SI-06", "SI-07", "SI-08", "SI-09", "BT-SI-09"], "SO": ["SO-02", "SO-03", "SO-01", "SO-04"]};


function get_data(){

	return data

}

function get_category_list(){

	return category_list
}
